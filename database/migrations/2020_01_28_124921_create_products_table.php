<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateProductsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('products', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->string('name')->nullable();
            $table->string('code')->nullable();
            $table->integer('required_coffee')->default(0);
            $table->integer('required_milk')->default(0);
            $table->integer('required_sugar')->default(0);
            $table->integer('serviced_count_all')->default(0);
            $table->integer('serviced_count_last')->default(0);
            $table->timestamps();
        });

        \Illuminate\Support\Facades\DB::table('products')->insert(
            array(
                'code' => 'espresso',
                'name' => 'Presszó kávé',
                'required_coffee' => 1,
                'required_milk' => 0,
                'required_sugar' => 0,
                'created_at' => \Carbon\Carbon::now(),
                'updated_at' => \Carbon\Carbon::now()
            )
        );

        \Illuminate\Support\Facades\DB::table('products')->insert(
            array(
                'code' => 'espresso_with_sugar',
                'name' => 'Presszó kávé cukorral',
                'required_coffee' => 1,
                'required_milk' => 0,
                'required_sugar' => 1,
                'created_at' => \Carbon\Carbon::now(),
                'updated_at' => \Carbon\Carbon::now()
            )
        );

        \Illuminate\Support\Facades\DB::table('products')->insert(
            array(
                'code' => 'espresso_with_milk',
                'name' => 'Presszó kávé tejjel',
                'required_coffee' => 1,
                'required_milk' => 1,
                'required_sugar' => 0,
                'created_at' => \Carbon\Carbon::now(),
                'updated_at' => \Carbon\Carbon::now()
            )
        );

        \Illuminate\Support\Facades\DB::table('products')->insert(
            array(
                'code' => 'espresso_with_milk_with_sugar',
                'name' => 'Presszó kávé tejjel és cukorral',
                'required_coffee' => 1,
                'required_milk' => 1,
                'required_sugar' => 1,
                'created_at' => \Carbon\Carbon::now(),
                'updated_at' => \Carbon\Carbon::now()
            )
        );

        \Illuminate\Support\Facades\DB::table('products')->insert(
            array(
                'code' => 'latte',
                'name' => 'Tejes kávé',
                'required_coffee' => 1,
                'required_milk' => 2,
                'required_sugar' => 1,
                'created_at' => \Carbon\Carbon::now(),
                'updated_at' => \Carbon\Carbon::now()
            )
        );

        \Illuminate\Support\Facades\DB::table('products')->insert(
            array(
                'code' => 'ristretto',
                'name' => 'Ristretto',
                'required_coffee' => 1,
                'required_milk' => 0,
                'required_sugar' => 0,
                'created_at' => \Carbon\Carbon::now(),
                'updated_at' => \Carbon\Carbon::now()
            )
        );
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('products');
    }
}
