<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateResourcesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('resources', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->string('name')->nullable();
            $table->string('code')->nullable();
            $table->integer('amount')->default(0);
            $table->integer('capacity')->default(0);
            $table->timestamps();
        });

        \Illuminate\Support\Facades\DB::table('resources')->insert(
            array(
                'code' => 'coffee',
                'name' => 'Kávé',
                'amount' => 10,
                'capacity' => 10,
                'created_at' => \Carbon\Carbon::now(),
                'updated_at' => \Carbon\Carbon::now()
            )
        );

        \Illuminate\Support\Facades\DB::table('resources')->insert(
            array(
                'code' => 'milk',
                'name' => 'Tej',
                'amount' => 10,
                'capacity' => 10,
                'created_at' => \Carbon\Carbon::now(),
                'updated_at' => \Carbon\Carbon::now()
            )
        );

        \Illuminate\Support\Facades\DB::table('resources')->insert(
            array(
                'code' => 'sugar',
                'name' => 'Cukor',
                'amount' => 10,
                'capacity' => 10,
                'created_at' => \Carbon\Carbon::now(),
                'updated_at' => \Carbon\Carbon::now()
            )
        );
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('resources');
    }
}
