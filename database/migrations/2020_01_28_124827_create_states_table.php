<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateStatesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('states', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->string('code')->nullable();
            $table->tinyInteger('value')->default(0);
            $table->timestamps();
        });

        \Illuminate\Support\Facades\DB::table('states')->insert(
            array(
                'code' => 'under_service',
                'value' => 0,
                'created_at' => \Carbon\Carbon::now(),
                'updated_at' => \Carbon\Carbon::now()
            )
        );

        \Illuminate\Support\Facades\DB::table('states')->insert(
            array(
                'code' => 'out_of_coffee',
                'value' => 0,
                'created_at' => \Carbon\Carbon::now(),
                'updated_at' => \Carbon\Carbon::now()
            )
        );

        \Illuminate\Support\Facades\DB::table('states')->insert(
            array(
                'code' => 'out_of_milk',
                'value' => 0,
                'created_at' => \Carbon\Carbon::now(),
                'updated_at' => \Carbon\Carbon::now()
            )
        );

        \Illuminate\Support\Facades\DB::table('states')->insert(
            array(
                'code' => 'out_of_sugar',
                'value' => 0,
                'created_at' => \Carbon\Carbon::now(),
                'updated_at' => \Carbon\Carbon::now()
            )
        );
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('states');
    }
}
