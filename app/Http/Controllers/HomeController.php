<?php

namespace App\Http\Controllers;

use App\Models\Product;
use App\Models\Resource;
use App\Models\State;
use Illuminate\Http\Request;
use RealRashid\SweetAlert\Facades\Alert;

class HomeController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        //$this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function index()
    {
        return view('home');
    }

    public function changeMachineState(Request $request){
        $state = $request->get('state');
        State::updateState('under_service', $state);
        if($state == 0) {
            Alert::success('Gép állapot változás', 'Sikeres átváltás szolgáltatás üzemre');
        }
        else{
            Alert::warning('Gép állapot változás', 'Sikeres átváltás szervíz üzemre');
        }
        return redirect()->back();
    }

    public function refillResource(Request $request){
        $code = $request->get('code');

        if($code == 'all'){
            $code = null;
        }

        $result = Resource::fillAmounts($code);
        $comment = '';
        for($i = 0; $i < count($result); $i++){
            $comment .= $result[$i] . '<br />';
        }

        Alert::html('Feltöltés sikeres', $comment, 'success');
        return redirect()->back();
    }

    public function serveProduct(Request $request){
        $code = $request->get('code');

        $product = Product::where('code', $code)->first();

        if($product) {
            if($product->getProductAvailability()) {
                Product::serviceProduct($product->id);
                Alert::success('Kávé elkészült!', 'Kérem vegye át az italt, és fogyassza egészséggel!');
            }
            else{
                Alert::warning('Kérés nem teljesthető!', 'Valamely hozzávaló elfogyott! Kérem válasszon másik terméket.');
            }
        }
        else{
            Alert::error('Kérés nem teljesthető!', 'A termék nem található.');
        }

        return redirect()->back();
    }
}
