<?php

namespace App\Console;

use App\Console\Commands\FillResources;
use App\Console\Commands\GetProductList;
use App\Console\Commands\GetServicedCoffees;
use App\Console\Commands\ServiceProduct;
use App\Console\Commands\SetOperationMode;
use App\Console\Commands\SetServiceMode;
use Illuminate\Console\Scheduling\Schedule;
use Illuminate\Foundation\Console\Kernel as ConsoleKernel;

class Kernel extends ConsoleKernel
{
    /**
     * The Artisan commands provided by your application.
     *
     * @var array
     */
    protected $commands = [
        SetServiceMode::class,
        SetOperationMode::class,
        GetServicedCoffees::class,
        GetProductList::class,
        ServiceProduct::class,
        FillResources::class
    ];

    /**
     * Define the application's command schedule.
     *
     * @param  \Illuminate\Console\Scheduling\Schedule  $schedule
     * @return void
     */
    protected function schedule(Schedule $schedule)
    {
        // $schedule->command('inspire')
        //          ->hourly();
    }

    /**
     * Register the commands for the application.
     *
     * @return void
     */
    protected function commands()
    {
        $this->load(__DIR__.'/Commands');

        require base_path('routes/console.php');
    }
}
