<?php

namespace App\Console\Commands;

use App\Models\Product;
use App\Models\State;
use Illuminate\Console\Command;

class SetServiceMode extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'machine:set-service-mode';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Change the coffee machine state to service mode';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        State::setServiceMode(true);
        $this->info('----------');
        $this->info('Az eszköz szervíz módba kapcsolt.');
        $this->info('----------');
        $this->info('Utolsó szervíz óta kiszolgált kávék:');
        $this->info('----------');
        $products = Product::all();
        foreach($products as $product){
            $this->info($product->getServicedLastString());
        }
    }
}
