<?php

namespace App\Console\Commands;

use App\Models\Product;
use Illuminate\Console\Command;

class GetProductList extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'machine:get-product-list';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'List the products and their actual availability';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        $products = Product::all();
        $this->info('----------');
        foreach($products as $product){
            $this->info($product->getProductAvailabilityString());
        }
        $this->info('----------');
    }
}
