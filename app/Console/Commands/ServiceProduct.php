<?php

namespace App\Console\Commands;

use App\Models\Product;
use App\Models\State;
use Illuminate\Console\Command;

class ServiceProduct extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'machine:service
                                    {code? : What type of coffee do you want? (See the options in product list)}';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Start service of a coffee by code';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        if(State::getStates()['under_service'] == 1){
            $this->warn('Szolgáltatás szervíz módban nem lehetséges! Kérem kapcsolja ki a szervíz módot az eszközön.');
            return false;
        }

        $product_code = $this->argument('code');
        if(!$product_code || $product_code == ''){
            $this->error('A code paraméter megadása kötelező!');
            return false;
        }

        $product = Product::where('code', $product_code)->first();
        if(!$product){
            $this->error('Nem található termék a megadott code paraméter alapján!');
            return false;
        }

        if(!$product->getProductAvailability()){
            $this->warn('A kiválasztott termék alapanyag hiány miatt nem rendelhető!');
            return false;
        }

        if(Product::serviceProduct($product->id)){
            $this->info('A ' . $product->name . ' elkészült, egészségére!');
        }
    }
}
