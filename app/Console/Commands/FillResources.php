<?php

namespace App\Console\Commands;

use App\Models\Resource;
use App\Models\State;
use Illuminate\Console\Command;

class FillResources extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'machine:fill-resources {code?}';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Refill the resources amount to maximum capacity, if code given, only that resource will be refilled';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        $this->info('----------');
        if(State::getStates()['under_service'] == 1) {


            $resource_code = $this->argument('code');
            if(!$resource_code || $resource_code == '' || is_null(Resource::where('code', $resource_code)->first())){
                $resource_code = null;
            }

            $result = Resource::fillAmounts($resource_code);
            for ($i = 0; $i < count($result); $i++) {
                $this->info($result[$i]);
            }
            $this->info('----------');
        }
        else{
            $this->warn('A tartályok feltöltése csak szervíz módban lehetséges! Kérem kapcsolja át az eszközt szervíz módra.');
        }
    }
}
