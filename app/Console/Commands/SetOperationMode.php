<?php

namespace App\Console\Commands;

use App\Models\State;
use Illuminate\Console\Command;

class SetOperationMode extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'machine:set-operation-mode';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Change the coffee machine state to operation mode';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        State::setServiceMode(false);
        $this->info('----------');
        $this->info('Az eszköz kilépett a szervíz módból.');
        $this->info('----------');
    }
}
