<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Arr;

class Resource extends Model
{
    protected $table = 'resources';

    protected $fillable = ['name', 'code', 'amount', 'capacity'];

    public static function getCoffeeAmount(){
        return Resource::where('code', 'coffee')->first()->amount;
    }

    public static function getMilkAmount(){
        return Resource::where('code', 'milk')->first()->amount;
    }

    public static function getSugarAmount(){
        return Resource::where('code', 'sugar')->first()->amount;
    }

    public static function decrementAmounts($product_id){
        $product = Product::where('id', $product_id)->first();
        $coffee = Resource::where('code', 'coffee')->first();
        $milk = Resource::where('code', 'milk')->first();
        $sugar = Resource::where('code', 'sugar')->first();

        $coffee->amount -= $product->required_coffee;
        $milk->amount -= $product->required_milk;
        $sugar->amount -= $product->required_sugar;

        $coffee->save();
        $milk->save();
        $sugar->save();

        if($coffee->amount == 0){
            State::updateState('out_of_coffee', 1);
        }

        if($milk->amount == 0){
            State::updateState('out_of_milk', 1);
        }

        if($sugar->amount == 0){
            State::updateState('out_of_sugar', 1);
        }
    }

    public static function fillAmounts($code = null){
        $result = [];

        if($code == null) {
            $coffee = Resource::where('code', 'coffee')->first();
            $milk = Resource::where('code', 'milk')->first();
            $sugar = Resource::where('code', 'sugar')->first();

            $coffee_missing = $coffee->capacity - $coffee->amount;
            $milk_missing = $milk->capacity - $milk->amount;
            $sugar_missing = $sugar->capacity - $sugar->amount;

            if ($coffee_missing > 0) {
                $coffee->amount = $coffee->capacity;
                $coffee->save();
                Event::createFillEvent($coffee_missing, $coffee->id);
                array_push($result, $coffee->name . ' feltöltve ' . $coffee_missing . ' egységgel.');
            } else array_push($result, $coffee->name . ' tárolóból nem fogyott egység.');

            if ($milk_missing > 0) {
                $milk->amount = $milk->capacity;
                $milk->save();
                Event::createFillEvent($milk_missing, $milk->id);
                array_push($result, $milk->name . ' feltöltve ' . $milk_missing . ' egységgel.');
            } else array_push($result, $milk->name . ' tárolóból nem fogyott egység.');

            if ($sugar_missing > 0) {
                $sugar->amount = $sugar->capacity;
                $sugar->save();
                Event::createFillEvent($sugar_missing, $sugar->id);
                array_push($result, $sugar->name . ' feltöltve ' . $sugar_missing . ' egységgel.');
            } else array_push($result, $sugar->name . ' tárolóból nem fogyott egység.');

            State::updateState('out_of_coffee', 0);
            State::updateState('out_of_milk', 0);
            State::updateState('out_of_sugar', 0);

            return $result;
        }
        else{
            $resource = Resource::where('code', $code)->first();
            $resource_missing = $resource->capacity - $resource->amount;

            if ($resource_missing > 0) {
                $resource->amount = $resource->capacity;
                $resource->save();
                Event::createFillEvent($resource_missing, $resource->id);
                array_push($result, $resource->name . ' feltöltve ' . $resource_missing . ' egységgel.');
            } else array_push($result, $resource->name . ' tárolóból nem fogyott egység.');

            State::updateState('out_of_' . $code, 0);

            return $result;
        }
    }

    public function getAmountInPercent(){
        return ($this->amount / $this->capacity) * 100;
    }
}
