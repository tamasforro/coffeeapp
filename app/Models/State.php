<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class State extends Model
{
    protected $table = 'states';

    protected $fillable = ['code', 'value'];

    public static function updateState($code, $value){
        $state = State::where('code', $code)->first();
        if($state){
            $state->value = $value;
            $state->save();
        }
    }

    public static function setServiceMode($service_mode = true){
        $state = State::where('code', 'under_service')->first();
        if($service_mode){
            $state->value = 1;
        }
        else{
            $state->value = 0;
            Product::initServicedCountLast();
        }

        $state->save();
    }

    public static function getStates(){
        return [
            'under_service' => State::where('code', 'under_service')->first()->value,
            'out_of_coffee' => State::where('code', 'out_of_coffee')->first()->value,
            'out_of_milk' => State::where('code', 'out_of_milk')->first()->value,
            'out_of_sugar' => State::where('code', 'out_of_sugar')->first()->value,
        ];
    }
}
