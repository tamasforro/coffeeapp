<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Product extends Model
{
    protected $table = 'products';

    protected $fillable = ['name', 'code', 'required_coffee', 'required_milk', 'required_sugar', 'serviced_count_all', 'serviced_count_last'];

    public static function serviceProduct($product_id = null){
        if(!is_null($product_id)){

            $product = Product::where('id', $product_id)->first();

            if($product->getProductAvailability()){
                Resource::decrementAmounts($product_id);
                Event::createServiceEvent($product_id);
                $product->incrementServicedCount();
                return true;
            }
        }
        return false;
    }

    public static function initServicedCountLast(){
        $products = Product::where('serviced_count_last', '>', 0)->get();
        foreach($products as $product){
            $product->serviced_count_last = 0;
            $product->save();
        }
    }

    public function incrementServicedCount(){
        $this->serviced_count_all += 1;
        $this->serviced_count_last += 1;
        $this->save();
    }

    public function getProductAvailability(){
        if(
            $this->required_coffee <= Resource::getCoffeeAmount() &&
            $this->required_milk <= Resource::getMilkAmount() &&
            $this->required_sugar <= Resource::getSugarAmount()
        ) return true;

        return false;
    }

    public function getServicedLastString(){
        return
            number_format($this->serviced_count_last)
            . ' - ' .
            $this->name
            . ' [' . Resource::where('code', 'coffee')->first()->name . ':' .
            number_format($this->required_coffee)
            . ' | ' . Resource::where('code', 'milk')->first()->name . ':' .
            number_format($this->required_milk)
            . ' | ' . Resource::where('code', 'sugar')->first()->name . ':' .
            number_format($this->required_sugar)
            . ']';
    }

    public function getProductAvailabilityString(){
        $available = 'Rendelhető';
        if(!$this->getProductAvailability()){
            $available = 'Nem rendelhető';
        }
        return
            $this->name
            . ' <code:' .
            $this->code
            . '> [' . Resource::where('code', 'coffee')->first()->name . ':' .
            number_format($this->required_coffee)
            . ' | ' . Resource::where('code', 'milk')->first()->name . ':' .
            number_format($this->required_milk)
            . ' | ' . Resource::where('code', 'sugar')->first()->name . ':' .
            number_format($this->required_sugar)
            . '] Készlet: ' .
            $available;
    }
}
