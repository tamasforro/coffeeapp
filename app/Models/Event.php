<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Event extends Model
{
    protected $table = 'events';

    protected $fillable = ['name', 'code', 'product_id', 'resource_id', 'amount'];

    public function product(){
        return $this->hasOne('App\Models\Product', 'id', 'product_id');

    }

    public function resource(){
        return $this->hasOne('App\Models\Resource', 'id', 'resource_id');
    }

    public static function createServiceEvent($product_id = null){
        if(!is_null($product_id)){
            $product = Product::where('id', $product_id)->first();

            $event = new Event();
            $event->code = 'service';
            $event->name = 'Kiszolgálás';
            $event->product_id = $product_id;
            $event->resource_id = Resource::where('code', 'coffee')->first()->id;
            $event->amount = (0 - $product->required_coffee);
            $event->save();

            if($product->required_milk > 0){
                $event = new Event();
                $event->code = 'service';
                $event->name = 'Kiszolgálás';
                $event->product_id = $product_id;
                $event->resource_id = Resource::where('code', 'milk')->first()->id;
                $event->amount = (0 - $product->required_milk);
                $event->save();
            }

            if($product->required_sugar > 0){
                $event = new Event();
                $event->code = 'service';
                $event->name = 'Kiszolgálás';
                $event->product_id = $product_id;
                $event->resource_id = Resource::where('code', 'sugar')->first()->id;
                $event->amount = (0 - $product->required_sugar);
                $event->save();
            }
        }
    }

    public static function createFillEvent($amount = 0, $resource_id = null){
        if(!is_null($resource_id) && $amount != 0){
            $event = new Event();
            $event->code = 'fill';
            $event->name = 'Feltöltés';
            $event->product_id = null;
            $event->resource_id = $resource_id;
            $event->amount = $amount;
            $event->save();
        }
    }
}
