## About CoffeeApp

CoffeeApp is an application created with Laravel framework, to fulfill a hiring task to a company.

- It is a virtual coffee machine;
- You can order different type of coffee products;
- You can change the machine to service mode, where you can manage the machine resources, and review the serviced products, event logs;
- You are allowed to make interactions from console with artisan commands, but it also has a clear frontend.

## Install CoffeeApp Application

In this section, I will describe how you can try and check the operation of this application.

1. Clone git repository to a specified directory
    - git clone https://tamasforro@bitbucket.org/tamasforro/coffeeapp.git

2. Generate app key    
    - php artisan key:generate --ansi

3. Install composer dependencies
    - composer install
    
4. Install node packages
    - npm install

5. Run Laravel Mix packager
    - npm run dev

6. Configure Apache local webserver host file, located usually %SYSTEM_DRIVE%:\xampp\apache\conf\extra\httpd-vhosts.conf
	- <VirtualHost *:80>
		 DocumentRoot "C:/xampp/htdocs/coffeeapp/public"
		 ServerName coffeeapp.local
	  </VirtualHost>

7. Append windows host file, located %SYSTEM_DRIVE%:\Windows\System32\drivers\etc\hosts
	- 127.0.0.1 coffeeapp.local
	
8. Start Apache Webserver

9. Start MySQL Database Service

10. Run database script to create the db
    - CREATE DATABASE `coffeeapp_db` /*!40100 COLLATE 'utf8mb4_general_ci' */

11. Modify your parameter in .ENV file (created from .ENV.EXAMPLE) "DB_DATABASE=coffeeapp_db"

12. Migrate tables
    - php artisan:migrate
    
13. Open web browser, and check application on location
    - http://coffeeapp.local/

## Artisan command descriptions

    php artisan machine:fill-resources {code?} 	Refill the resources amount to maximum capacity, if code given, only that resource will be refilled
    php artisan machine:get-product-list 		List the products and their actual availability
    php artisan machine:get-serviced-coffees	Get the serviced product counts from the last service
    php artisan machine:service {code?}		    Start service of a coffee by code
    php artisan machine:set-operation-mode		Change the coffee machine state to operation mode
    php artisan machine:set-service-mode		Change the coffee machine state to service mode

## Code dictionary

Product list:

    - Presszó kávé                      <espresso>
    - Presszó kávé cukorral             <espresso_with_sugar>
    - Presszó kávé tejjel               <espresso_with_milk>
    - Presszó kávé tejjel és cukorral   <espresso_with_milk_with_sugar>
    - Tejes kávé                        <latte>
    - Ristretto                         <ristretto>

Resource list:

    - Kávé  <coffee>
    - Tej   <milk>
    - Cukor <sugar>
    
## Development details

Timing:

    - Plan models                       10 minutes
    - Implement models                  30 minutes
    - Create models functions           60 minutes
    - Create artisan commands           60 minutes
    - Plan frontend                     20 minutes
    - Implement controller functions    20 minutes
    - Implement frontend                90 minutes
    - Test features                     60 minutes
    - Write documentation               60 minutes
    ------SUM------                     410 minutes = 6-7 hours      
