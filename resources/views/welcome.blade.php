@extends('layouts.app')

@section('content')
    <div class="container">
        <div class="row justify-content-center">
            <div class="col-md-8">
                <div class="card">
                    <div class="card-header">Kávégép</div>

                    <div class="card-body">
                        <h3>Kávé gép</h3>
                        <p>
                            A gép státusza:
                            @if(\App\Models\State::getStates()['under_service'] == 1)
                                <strong class="label-error">
                                    Szervíz üzem
                                </strong>
                                <br />
                                <br />
                                <a class="btn btn-success" href="{{ route('change-state', ['state' => 0]) }}">Váltás szolgáltatás üzemre</a>
                            @else
                                <strong class="label-success">
                                    Szolgáltatás üzem
                                </strong>
                                <br />
                                <br />
                                <a class="btn btn-danger" href="{{ route('change-state', ['state' => 1]) }}">Váltás szervíz üzemre</a>
                            @endif
                        </p>
                    </div>
                </div>
            </div>
        </div>
    </div>


    <div class="container">
        <div class="row justify-content-center">
            <div class="col-md-8">
                @if(\App\Models\State::getStates()['under_service'] == 1)

                    <div class="card">
                        <div class="card-header">Szervízfunciók</div>
                        <div class="card-body">
                            <h4>Tartályok állása</h4>
                            @foreach(\App\Models\Resource::all() as $resource)
                                <div class="row resource-row justify-content-center align-self-center">

                                    <div class="col-md-4 text-right align-self-center">
                                        {{ $resource->name }}
                                    </div>
                                    <div class="col-md-4 align-self-center">
                                        <div class="progress">
                                            <div class="progress-bar
                                                @if($resource->getAmountInPercent() <= 25)
                                                    bg-danger
                                                @elseif($resource->getAmountInPercent() <= 50 && $resource->getAmountInPercent() > 25)
                                                    bg-warning
                                                @elseif($resource->getAmountInPercent() <= 75 && $resource->getAmountInPercent() > 50)
                                                    bg-info
                                                @elseif($resource->getAmountInPercent() <= 100 && $resource->getAmountInPercent() > 75)
                                                    bg-success
                                                @endif
                                                    " style="width:{{ $resource->getAmountInPercent() }}%"></div>
                                        </div>
                                    </div>
                                    <div class="col-md-4 text-left">
                                        <a class="btn btn-sm btn-primary" href="{{ route('refill-resource', ['code' => $resource->code]) }}">Feltöltés</a>
                                    </div>
                                </div>
                            @endforeach

                                <div class="row resource-row justify-content-center align-self-center">
                                    <div class="col-md-4 text-right align-self-center">
                                    </div>
                                    <div class="col-md-4 align-self-center text-center">
                                        <a class="btn btn-sm btn-outline-secondary" href="{{ route('refill-resource', ['code' => 'all']) }}">Összes feltöltése</a>
                                    </div>
                                    <div class="col-md-4 text-left">
                                    </div>
                                </div>


                                <div class="row justify-content-center align-self-center">
                                    <div class="col-md-12">
                                        <h4>Kiszolgált kávék</h4>
                                        <table class="table table-striped table-responsive">
                                            <thead>
                                                <th>Darab</th>
                                                <th>Típus</th>
                                                <th>Kávé</th>
                                                <th>Tej</th>
                                                <th>Cukor</th>
                                                <th>Elérhető?</th>
                                            </thead>
                                            <tbody>
                                                @foreach(\App\Models\Product::all() as $product)
                                                    <tr>
                                                        <td>{{ $product->serviced_count_last }}</td>
                                                        <td>{{ $product->name }}</td>
                                                        <td>{{ $product->required_coffee }}</td>
                                                        <td>{{ $product->required_milk }}</td>
                                                        <td>{{ $product->required_sugar }}</td>
                                                        @if($product->getProductAvailability())
                                                            <td><span class="badge badge-success">Igen</span></td>
                                                        @else
                                                            <td><span class="badge badge-danger">Nem</span></td>
                                                        @endif
                                                    </tr>
                                                @endforeach
                                            </tbody>
                                        </table>

                                    </div>
                                </div>


                            <div class="row justify-content-center align-self-center">
                                <div class="col-md-12">
                                    <h4>Esemény napló</h4>
                                    <table class="table table-striped table-responsive">
                                        <thead>
                                        <th>Típus</th>
                                        <th>Termék</th>
                                        <th>Hozzávaló</th>
                                        <th>Kapacitás</th>
                                        <th>Időpont</th>
                                        </thead>
                                        <tbody>
                                        @foreach(\App\Models\Event::orderBy('id', 'desc')->get() as $event)
                                            <tr>
                                                <td>{{ $event->name }}</td>
                                                @if($event->product()->count() > 0)
                                                    <td>{{ $event->product()->first()->name }}</td>
                                                @else
                                                    <td>N/A</td>
                                                @endif

                                                @if($event->resource()->count() > 0)
                                                    <td>{{ $event->resource()->first()->name }}</td>
                                                @else
                                                    <td>N/A</td>
                                                @endif

                                                <td>{{ $event->amount }}</td>
                                                <td>{{ $event->created_at }}</td>
                                            </tr>
                                        @endforeach
                                        </tbody>
                                    </table>

                                </div>
                            </div>
                        </div>
                    </div>
                @else
                    <div class="card">
                        <div class="card-header">Szolgáltatások</div>
                        <div class="card-body">
                            <div class="row">
                                <div class="col-md-12">
                                    <h4>Választható termékek</h4>
                                    @foreach(\App\Models\Product::all() as $product)
                                        <div class="col-md-12 text-center">
                                        @if($product->getProductAvailability())
                                            <button
                                                class="btn btn-sm btn-primary product-button"
                                                data-coffee="{{ $product->required_coffee }}"
                                                data-milk="{{ $product->required_milk }}"
                                                data-sugar="{{ $product->required_sugar }}"
                                                data-name="{{ $product->name }}"
                                                data-route="{{ route('serve-product', ['code' => $product->code ]) }}"
                                                id="{{ $product->id }}"
                                                onclick="serve({{ $product->id }})">
                                                {{ $product->name }}
                                            </button>
                                        @else
                                            <button class="btn btn-sm btn-primary product-button disabled">
                                                {{ $product->name }}
                                            </button>
                                        @endif
                                        </div>
                                    @endforeach
                                </div>
                            </div>
                            <div class="row service-progress">
                                <div class="col-md-12 align-self-center">
                                    <h4>Termék készítése: <span id="service-name"></span></h4>
                                    <span id="service-step"> </span>
                                    <div class="progress">
                                        <div class="progress-bar bg-primary" id="service-progress" style="width:1%"></div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                @endif
            </div>
        </div>
    </div>

@endsection
