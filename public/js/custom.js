function serve(id){
    var coffee = $('#' + id).data('coffee');
    var milk = $('#' + id).data('milk');
    var sugar = $('#' + id).data('sugar');
    var name = $('#' + id).data('name');
    var route = $('#' + id).data('route');

    var ingredients = ['Pohár előkészítése...', 'Víz forralása...', 'Kávé örlése...'];
    if(coffee > 0){
        ingredients.push('Kávé főzése...');
    }
    if(milk > 0){
        ingredients.push('Tej melegítése...');
        ingredients.push('Tej hozzáadása...');
    }
    if(sugar > 0){
        ingredients.push('Cukor hozzáadása...');
    }

    $('.product-button').each(function(){
        $(this).prop('disabled', true);
    })

    $('.service-progress').css('display', 'block');
    $('#service-name').text(name);
    $('#service-step').text(ingredients[0]);

    var step_size = 100 / ingredients.length;


    var i = 0;
    var step = 0;
    var length = 100;
    move();
    function move() {
        if (i === 0) {
            i = 1;
            var elem = document.getElementById("service-progress");
            $('#service-step').text(ingredients[step]);
            var width = 1;
            var id = setInterval(frame, length);
            function frame() {
                $('#service-step').text(ingredients[step]);
                if (width >= 100) {
                    clearInterval(id);
                    i = 0;
                    window.location = route;
                } else {
                    if(width > step_size * step)
                    {
                        step++;
                    }
                    width++;
                    elem.style.width = width + "%";
                }
            }
        }
    }
}


